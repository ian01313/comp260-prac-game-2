﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class AIPaddle : MonoBehaviour {

	private new Rigidbody rigidbody;

	void Start () {
		rigidbody = GetComponent <Rigidbody> ();
		rigidbody.useGravity = false;
	}

	public LayerMask TargetPuck;

	public float force = 10f;

	void FixedUpdate (){
		if (GameObject.Find ("Puck").transform.position.x < rigidbody.position.x) {
			Vector3 dir = GameObject.Find ("Puck").transform.position - rigidbody.position;
			rigidbody.AddForce (dir.normalized * force);
		} else {
			Vector3 dire = GameObject.Find ("Puck").transform.position - rigidbody.position;
			rigidbody.AddForce (dire.normalized * force);
		}
	}
}
