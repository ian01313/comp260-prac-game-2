﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

	public AudioClip wallCollideClip;
	private new AudioSource audio;
	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;
	public Transform startingPos;
	private new Rigidbody rigidbody;

	void Start () {
		audio = GetComponent<AudioSource> ();
		rigidbody = GetComponent<Rigidbody> ();
		ResetPosition ();
	
	}

	public void ResetPosition () {
		//teleport to the starting position
		rigidbody.MovePosition(startingPos.position);
		//stop it from moving
		rigidbody.velocity = Vector3.zero;
	}

	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter (Collision collision) {
		//check what we have hit
		if (paddleLayer.Contains (collision.gameObject)) {
			audio.PlayOneShot (paddleCollideClip);
		//	Debug.Log ("Collision Enter" + collision.gameObject.name);
		} else {
			// hit something else
			audio.PlayOneShot (wallCollideClip);
		}
	}

	void OnCollisionStay (Collision collision) {
	//	Debug.Log ("Collision Stay" + collision.gameObject.name);
	}

	void OnCollisionExit (Collision collision) {
	//	Debug.Log ("Collision Exit" + collision.gameObject.name);
	}
}
