﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Rigidbody))]
public class PlayerMoveKeyboard : MonoBehaviour {

	private new Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent <Rigidbody> ();
		rigidbody.useGravity = false;
	}

	public Vector2 move;
	public Vector2 velocity; //in metres per second

	public float speed = 20f;
	public float force = 10f;
	// Update is called once per frame
	void Update () {
		Vector2 direction;
		direction.x = Input.GetAxis ("Horizontal");
		direction.y = Input.GetAxis ("Vertical");

		//scale the velocity by the time frame
		//Vector2 velocity = direction * Time.deltaTime;
		Vector2 velocity = direction * force * speed;
		//move the object
		rigidbody.AddForce(velocity * Time.deltaTime);

		//transform.Translate(velocity * Time.deltaTime);
	}
}
