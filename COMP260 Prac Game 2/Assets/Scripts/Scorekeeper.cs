﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Scorekeeper : MonoBehaviour {

	public int scorePerGoal = 1;
	private int[] score = new int [2];
	public Text[] scoreText;
	public bool player1Wins = false;
	public bool player2Wins = false;
	public Text winText;

	void Start () {
	//subscribe to events from all the Goals
		Goal[] goals = FindObjectsOfType<Goal> ();
		Time.timeScale = 1;
		winText.text = "";

		for (int i = 0; i < goals.Length; i++) {
			goals [i].scoreGoalEvent += OnScoreGoal;
		}
			//reset the scores to zero
			for(int i = 0; i <score.Length; i++) {
				score[i] = 0;
				scoreText [i].text = "0";
			}
	}

	public void OnScoreGoal (int player) {
		//add points to the player whose goal it is
		score[player] += scorePerGoal;
		scoreText[player].text = score[player].ToString();
		Debug.Log ("Player" + player + ":" + score [player]);

		if (scoreText [0].text == "10") {
			player1Wins = true;
			Debug.Log ("Player 1 wins");
			winText.text = "You are Winner P1";
			Time.timeScale = 0;
		} else {
			player2Wins = true;
			Debug.Log ("Player 2 wins");
			winText.text = "You are Winner P2";
			Time.timeScale = 0;
			}
	}
}
